<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Professional;
use Illuminate\Support\Facades\Hash;
class ProfessionalsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_account= Professional::factory()->create([
            'first_name'=>'George',
            'last_name'=>'Gathura',
            'email_address'=>'george@teamgigo.com',
            'alternative_email_address'=>'gkwambata@yahoo.com',
            'password'=>Hash::make('password')
        ]);
        Professional::factory()->count(500)->create();
    }
}
