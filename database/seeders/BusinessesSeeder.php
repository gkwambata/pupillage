<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Premium\Business;
use App\Models\Professional;

class BusinessesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_profile=Professional::where('email_address','george@teamgigo.com')->first();
        $test_account = Business::factory()->create([
            'business_name'=>'TeamGigo',
            'professional_id'=>$test_profile->id,
        ]);

        Business::factory()->count(499)->create();
    }
}
