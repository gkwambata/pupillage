<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Premium\Event;
use App\Models\Professional;
use App\Models\Premium\Business;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $professional = Professional::where('email_address','george@teamgigo.com')->get()->first();
        $business = Business::where('professional_id',$professional->id)->first();
        $test_events = Event::factory()->count(500)->create([
            'business_id'=>$business->id
        ]);
        
    }
}
