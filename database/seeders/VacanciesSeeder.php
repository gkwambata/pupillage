<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Premium\Business;
use App\Models\Premium\Vacancy;
use App\Models\Professional;

class VacanciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $professional = Professional::where('email_address','george@teamgigo.com')->get()->first();
        $business = Business::where('professional_id',$professional->id)->first();
        $test_events = Vacancy::factory()->count(500)->salary_range()->reviewed()->create([
            'business_id'=>$business->id
        ]);
        
    }
}
