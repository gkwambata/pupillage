<?php

namespace Database\Factories;

use App\Models\Professional;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProfessionalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Professional::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name'=>$this->faker->name(),
            'last_name'=>$this->faker->name(),
            'email_address'=>$this->faker->unique()->safeEmail(),
            'alternative_email_address'=>$this->faker->unique()->safeEmail(),
            'linkedin_profile'=>$this->faker->url().str::random(10),
            'phone_number'=>'+254719726507',
            'phone_number_verified_at'=>now(),
            'email_verified_at'=>now(),
            'alternate_email_verified_at'=>now(),
            'password'=>'$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => Str::random(10),
            'last_authorized'=>now(),
            'last_accessed'=>now(),

        ];
    }

    /**
     * Helps created model definitions that are unverified for database testing
     * 
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified(){

        return $this->state( function(array $attributes){
            return[
                'email_verified_at'=>NULL,
                'alternate_email_verified_at'=>NULL,
                'phone_number_verified_at'=>NULL
            ];
        });

    }
}
