<?php

namespace Database\Factories\Premium;

use App\Models\Premium\Vacancy;
use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\User;
use App\Models\Premium\Business;
use App\Models\Professional;

class VacancyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vacancy::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            "business_id"=> Business::factory(),
            "job_title"=>$this->faker->name(),
            "job_location"=>$this->faker->word(),
            "description"=>$this->faker->sentence(),
            "application_instructions"=>$this->faker->paragraph(5),
            "start_date"=>now()->add(30,'day'),
            "deadline"=>now()->add(60,'day'),
            "created_by"=>Professional::factory(),
            "reviewed_by"=>User::factory(),
            "expires_at"=>now()->add(90,'day'),
            "reviewed"=>0,
        ];
            
    }


    public function reviewed()
    {
        return $this->state(function(array $attributes){
            return[
                'reviewed'=>1
            ];
        });
    }
    public function salary_range()
    {
        return $this->state(function(array $attributes){
            return[
                'salary_range_min'=>floatval(100000),
                'salary_range_max'=>floatval( rand(110000,190000))
            ];
        });
    }
}
