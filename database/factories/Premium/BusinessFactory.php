<?php

namespace Database\Factories\Premium;

use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Premium\Business;
use App\Models\Professional;
class BusinessFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Business::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "business_name"=>$this->faker->name(),
            "history"=>$this->faker->paragraph(5),
            'professional_id'=>Professional::factory(),
            "alternative_email"=>$this->faker->unique()->safeEmail(),
            "alternative_phone"=>'+254'.rand(100,999).'726'.rand(200,999),
      
        ];
    }

    public function validated_account()
    {
        return $this->state(function(array $attributes){
            return [
                "validated_phone"=>1,
                "validated_dns"=>1,
                "validated_email"=>1,
                "validation_date"=>now()
            ];
        });
    }
    public function validated_dns()
    {
        return $this->state(function(array $attributes){
            return [
                "validated_dns"=>1
            ];
        });
    }

    public function validated_phone()
    {
        return $this->state(function(array $attributes){
            return [
                "validated_phone"=>1
            ];
        });
    }



}
