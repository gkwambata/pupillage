<?php

namespace Database\Factories\Premium;

use App\Models\Premium\Event;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Premium\Business;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "business_id"=>Business::factory(),
            "event_title"=>$this->faker->words(4,true),
            "description"=>$this->faker->paragraph(10),
            "start_date"=>now()->add( rand(5,20),'day'),
            "end_date"=>now()->add( rand(21,45),'day'),
            "event_url"=>$this->faker->url(),
        ];
    }

    /**
     * Indicate that the event already happenned.The default definition schedules events.
     * This method provides a means of creating past definitions for testing
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function past_event()
    {
        return $this->state(function (array $attributes) {
            return [
                "start_date" => now()->subtract( rand(5,20),'day'),
                "end_date"=>now()->subtract( rand(21,45),'day'),
            ];
        });
    }




}
