<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->id();
            $table->string("business_name");
            $table->text("history")->nullable();
            $table->foreignId('professional_id')->constrained();
            $table->string("alternative_email")->unique();
            $table->string("alternative_phone")->unique();
            $table->boolean("validated_dns")->default(0);
            $table->boolean("validated_phone")->default(0);
            $table->boolean("validated_email")->default(0);
            $table->dateTime("validation_date")->nullable();
           // $table->foreignId('user_id')->constrained()->nullable();
            $table->softDeletes();
 
            //billing_id,package_id,last_subscription,
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
