<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professionals', function (Blueprint $table) {
            $table->id();
            //$table->uuid('access_id')->unique();
            $table->string("first_name");
            $table->string("last_name");
            $table->string("email_address")->unique();
            $table->string("alternative_email_address")->unique();
            $table->string("linkedin_profile")->unique(); #Avoid fraudlent/impersonations
            $table->string("phone_number");
            $table->timestamp("phone_number_verified_at")->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('alternate_email_verified_at')->nullable();
            $table->string('password');
            $table->date("last_authorized");
            $table->date("last_accessed");
            $table->rememberToken();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professionals');
    }
}
