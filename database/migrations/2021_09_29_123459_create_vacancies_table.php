<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('business_id')->constrained();
            $table->string("job_title");
            $table->string("job_location")->nullable();
            $table->text("description");
            $table->double("salary_range_min",$precision = 13, $scale = 2)->nullable();
            $table->double("salary_range_max",$precision = 13, $scale = 2)->nullable();
            $table->text("application_instructions");
            $table->dateTime("start_date");
            $table->date("deadline");//application_deadline
            $table->unsignedBigInteger('created_by');
            $table->foreign('created_by')->references('id')->on('professionals');
            $table->date("expires_at");
          
            $table->boolean("reviewed")->default(0);
            $table->unsignedBigInteger('reviewed_by');
            $table->foreign('reviewed_by')->references('id')->on('users')->nullable();
            $table->date("review_date")->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
