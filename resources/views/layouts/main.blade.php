<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="A Space for providing Policy & Ruling Updates Affecting Economies Within East Africa"/>
    <title>PolicyEA - @yield('pageTitle',"Home") </title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	  
    @livewireStyles

    <style>
        body{
            /* font-family:serif; */
        }
        #main_nav ul li{
              display: inline;
        }

       #app_title{
            margin:0 auto;
            width:300px;
        }
        #main_nav,#navlinks{
            text-align:center;
            margin:0 auto;
           
        }

        #main_nav a{
            color:#0a5a9c;
            font-weight:bold;
        }
        #man_nav a:hover{
            /* text-decoration:underline; */
            border-bottom:4px solid #000;
        }
        #main_nav a:visited{
            color:#004a7f;
            border-bottom:4px solid #000;
        }
        .timetable .time{
            display:block;
        }
        .timetable .desc a{
            display:block;
            width:150px;
      
        }
        .timetable .time span{
            color:rgb(79, 70, 229 );
            font-weight:700;
            border-right:2px solid rgb(79,70,229);
            padding-right:10px;
        }
    </style>
    </head>
<body class="bg-gray-100">
<livewire:notices />
<div class="container mx-auto shadow-lg rounded-lg py-8 my-8">
    <nav role="navigation" id="main_nav">
        <div id="app_title" class="w-36">
           <h2 class="font-extrabold text-xl">PolicyEA</h2>
           <span class="font-semibold text-lg">Digital Space for Policy discussions affecting East Africa</span>
        </div>
        <ul id="navlinks" class="border-t-2 border-b-2 p-3 mt-10 text-lg">
            <li class="px-2" ><a href="{{url('/')}}">News</a></li>
            <li class="px-2" ><a href="{{url('jobs')}}">Jobs</a></li>
            <li class="px-2" ><a href="{{url('events')}}">Events</a></li>
            <li class="px-2" ><a href="{{url('faq')}}">FAQ</a></li>
            <li class="px-2"><a href="{{url('contact-us')}}">Contact us</a></li>
            
        </ul>
       
    </nav>

    <main>
        @yield('content')
    </main>

</div>


<script href="{{ asset('js/app.js') }}" ></script>

@livewireScripts

</body>

</html>