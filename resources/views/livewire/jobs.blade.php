<!-- This example requires Tailwind CSS v2.0+ -->
<div class="">
  <div class="max-w-7xl mx-auto py-8 px-4 sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center lg:justify-between">
    <h2 class="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">
      <span class="block">Advertise your event?</span>
      <span class="block text-indigo-600">Start your free trial today.</span>
    </h2>
    <div class="mt-8 flex lg:mt-0 lg:flex-shrink-0">
      <div class="inline-flex rounded-md shadow">
        <a href="#" class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700">
          Get started
        </a>
      </div>
      <div class="ml-3 inline-flex rounded-md shadow">
        <a href="#" class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-indigo-600 bg-white hover:bg-indigo-50">
          Learn more
        </a>
      </div>
    </div>
  </div>

  <div class="timetable">
          @if($jobs)
            @foreach($jobs as $job)
                  <div class="time border-t-2 border-b-2 py-12">
                      <div class="px-10 ">
                        @php
                          $start_date=strtotime($job->start_date);
                          $application_date = date('d F Y',$start_date);
                          $application_date_end = date('d F Y', strtotime($job->deadline) );
                        @endphp
                        <span> Job: {{ucfirst($job->job_title) }}</span> &nbsp;
                        <span>Firm: {{ $job->business->business_name}}</span> <br/>
                        
                        <span> From : {{ $application_date }} </span> &nbsp; <span>To : {{ $application_date_end}}</span>
                      
                      </div>
                      <div class="desc px-10">
                        {{$job->description }}
                        <a class="mt-2 items-center justify-center px-5 py-3 border-solid border-2 text-base font-medium rounded-md text-black" href="{{$event->event_url}}">Click Here</a>
                      </div>
                    </div>
            @endforeach
          @endif
  </div>


</div>
