<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Testing all possible client-facing/front-end uris
 *
 * 
 */
class VisitorRequestsTest extends TestCase
{

    public function test_home_page()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_events_page()
    {
        $response = $this->get('/events');

        $response->assertStatus(200);
    }
    public function test_jobs_page()
    {
        $response = $this->get('/jobs');

        $response->assertStatus(200);
    }

    public function test_login()
    {
        $response = $this->get('/login');

        $response->assertStatus(404);
    }

    public function test_faq_page()
    {
        $response = $this->get('/faq');

        $response->assertStatus(200);
    }

    public function test_contact_page()
    {
        $response = $this->get('/contact-us');

        $response->assertStatus(200);
    } 

}
