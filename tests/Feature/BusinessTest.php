<?php

use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Premium\Business;

use App\Models\Professional;

uses(RefreshDatabase::class);

test('assert model can be created',function(Business $business){

        $this->assertDatabaseCount('businesses', 1);
})->with([
    fn()=> Business::factory()->create()
]);

test('assert model can be searched',function(){
    $this->assertDatabaseHas('businesses', [
        'alternative_phone' => '+255716524357',
    ]);     
})->with([
    fn()=> Business::factory()->create(['alternative_phone'=>'+255716524357'])
]);

test('assert model can be validated',function(){
    
    $this->assertDatabaseHas('businesses', [
        'validated_email' => 1,
        'validated_dns'=>1,
        'validated_phone'=>1
    ]);     
})->with([
    fn()=>Business::factory()->validated_account()->create()
]);

test('assert model is by default not validated',function(){
    
    $this->assertDatabaseMissing('businesses', [
        'validated_email' => 1,
        'validated_dns'=>1,
        'validated_phone'=>1
    ]);     
})->with([
    fn()=>Business::factory()->create()
]);

test('assert model can be deleted',function(Business $business){

    $business->delete();
    $this->assertSoftDeleted($business);  
})->with([
    fn()=> Business::factory()->create()
]);