<?php 
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Models\Professional;

uses(RefreshDatabase::class);


test('assert model can be created',function(){
   $this->assertDatabaseCount('professionals', 1);
})->with([
    fn()=>Professional::factory()->create()
]);

test('assert model can be searched',function(Professional $professional){
        $this->assertDatabaseHas('professionals', [
        'phone_number' => '+254719726507',
        ]);      
})->with([
    fn()=>Professional::factory()->create()
]);


test('assert model can be deleted',function(Professional $professional){
    $professional->delete();
    $this->assertSoftDeleted($professional);

})->with([
    fn()=>Professional::factory()->create()
]);

/**
 * Testing Table architecture. Should allow creation of model before verification process
 */

test('assert model is incomplete',function(){
        $this->assertDatabaseHas('professionals',[
            'email_verified_at'=>NULL,
            'phone_number_verified_at'=>NULL,
            'alternate_email_verified_at'=>NULL
        ]);
})->with([
    fn()=>Professional::factory()->unverified()->create()
]);

test('assert model is complete',function(){
        $this->assertDatabaseMissing('professionals',[
            'email_verified_at'=>NULL,
            'phone_number_verified_at'=>NULL,
            'alternate_email_verified_at'=>NULL
        ]);
});