<?php
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Premium\Vacancy;


uses(RefreshDatabase::class);

test('assert model be created',function(){
    $vacancy = Vacancy::factory()->count(3)->create();

    expect($vacancy)->toHaveCount(3);
      
});

test('assert model be searched',function(){
   $this->assertDatabaseHas('vacancies',[
       'job_title'=>'Superman'
   ]);
      
})->with([
    fn()=>Vacancy::factory()->count(3)->reviewed()->salary_range()->create(['job_title'=>'Superman','job_location'=>'Nairobi Kenya'])
]);

test('assert model has no defined salary',function(){
   $this->assertDatabaseHas('vacancies',[
       'salary_range_min'=>NULL,
       'salary_range_max'=>NULL
   ]);
      
})->with([
    fn()=>Vacancy::factory()->count(3)->reviewed()->create(['job_title'=>'Superman','job_location'=>'Nairobi Kenya'])
]);

test('assert model has salary',function(){
   $this->assertDatabaseMissing('vacancies',[
       'salary_range_min'=>NULL,
       'salary_range_max'=>NULL
   ]);
      
})->with([
    fn()=>Vacancy::factory()->count(3)->salary_range()->create()
]);

test('assert model has been reviewed',function(){
   $this->assertDatabaseHas('vacancies',[
       'reviewed'=>1,
   ]);
      
})->with([
    fn()=>Vacancy::factory()->count(3)->reviewed()->create()
]);

