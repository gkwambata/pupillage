<?php
use App\Models\Premium\Event;
 
use Illuminate\Foundation\Testing\RefreshDatabase;


// Uses the given trait in the current file
uses(RefreshDatabase::class);
 
it('can create model',function(){

    $event = Event::factory()
                    ->count(3)->create();

    
    expect($event)->toHaveCount(3);
});

it('can search model',function(Event $event){
   expect($event->event_title)->toBe("{$event->event_title}");
})->with([
     fn() => Event::factory()
                    ->create(['event_title' => 'Nuno', 'event_url' => 'https://pupillageurl.com/events'])
]);

it('can delete model',function(Event $event){
   $event->delete();
   $this->assertSoftDeleted($event);
})->with([
     fn() => Event::factory()
                    ->create(['event_title' => 'Nuno', 'event_url' => 'https://pupillageurl.com/events'])
]);