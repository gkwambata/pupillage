<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Premium\Vacancy;

class Jobs extends Component
{
    public $jobs;

    public function render()
    {
        #$this->jobs = Vacancy::with('business: id,business_name')->get();
        return view('livewire.jobs');
    }
}
