<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Premium\Event;
use Illuminate\Support\Facades\Redis;

class Events extends Component
{
    public $events;
    public function render()
    {   
        $this->events = Event::with('business:id,business_name')->limit(20)->get();        
        return view('livewire.events');
    }
}
