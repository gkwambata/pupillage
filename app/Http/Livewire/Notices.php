<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Notices extends Component
{
    public function render()
    {
        return view('livewire.notices');
    }
}
