<?php

namespace App\Models\Premium;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Premium\Business;

class Event extends Model
{
    use HasFactory,SoftDeletes;

    public function business()
    {
        return $this->belongsTo(Business::class);
    }
}
