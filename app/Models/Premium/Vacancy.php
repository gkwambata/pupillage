<?php

namespace App\Models\Premium;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Premium\Business;
use App\Models\Professional;
use App\Models\User;

class Vacancy extends Model
{
    use HasFactory,SoftDeletes;

    public function business()
    {
        $this->belongsTo(Business::class);
    }

    public function professional()
    {
        $this->belongsTo(Professional::class);
    }

    public function user()
    {
        $this->belongsTo(User::class);
    }

}
