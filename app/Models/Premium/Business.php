<?php

namespace App\Models\Premium;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\{
    Professional,
    Premium\Event,
    Premium\Vacancy
};

class Business extends Model
{
    use HasFactory,SoftDeletes;

    /**
     * Get the professional that is responsible for the business.
     */
    public function professional()
    {
        return $this->belongsTo( Professional::class );
    }

    /**
     * Get the events posted by business.
     */
    public function events()
    {
        return $this->hasMany( Event::class );
    }

    /**
     * Get the vacancies made available by business.
     */
    public function vacancies()
    {
        return $this->hasMany(Vacancy::class);
    }
}
