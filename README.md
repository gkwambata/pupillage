# PolicyEA: Platform for Policy Experts to Engage on policy
<p align="center">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About PolicyEA

A platform for policy experts to earn and contribute well informed opinions on policies shaping East Africa. We aim to be a reliable source of information for corporates and interested parties on the state of policies made within East Africa.

To generate revenue, we provide a space where events & vacancies in law firms are advertised within East Africa. The vacancy/events modules offer a way for law firms to filter/direct applications from law practitioners/graduates to channels they would prefer i.e either through this platform or with detailed instructions on how to apply.

Features
- Policy Blog & Forum
- Membership areas for [applicants](https://policyea.ea/applicants) and [law firms](https://policyea.ke/business)
- API endpoints for [Mobile based applications & Partnerships](https://policyea.ke/thirdparty)


## Original Maintainer

- **[George@TeamGigo](https://teamgigo.com/)**

## Security Vulnerabilities

If you discover a security vulnerability within this repo, please send an e-mail to George Gathura via [george@teamgigo.com](mailto:george@teamgigo.com). All security vulnerabilities will be promptly addressed.

## License

This is a private repository designed and copyrighted to [teamgigo](https://teamgigo.com)
